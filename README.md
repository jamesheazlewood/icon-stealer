# Icon Stealer

## Intro

This small 1-page script sifts through multiple icon websites, including Google Images, and displays them in a list for inspiration / stealing purposes. 

## Usage

To use this app properly, you need to run Chrome in "no security" mode. Even though it will warn you about it, it needs to allow access to CORS from the script locally. The easiest way to do this is to run Chrome from a command line with the no security flag switched.

For Mac:

```bash
open -n -a "/Applications/Google Chrome.app" "${window.location.href}" --args --user-data-dir="/tmp/chrome_dev_session" --disable-web-security --start-maximized
```
For Windows:
```bash
"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe" "${window.location.href}" --args --disable-web-security --start-maximized --user-data-dir="C:\\tmp\\Chrome Dev Session"
```

## Trouble Shooting and FAQ
Nobody has asked any questions yet.